import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        //    Create a arraylist to contain the 2 hands
        ArrayList<Queue<Card>> hands = new ArrayList<Queue<Card>>();
        //add the 2 hands into the hands arraylist
        hands.add(new Queue<Card>());
        hands.add(new Queue<Card>());
        //deal the cards
        deal(hands);
        //play the game
        playWar(hands);
    }

    //deal card method
    static void deal(ArrayList<Queue<Card>> hands) {
        //create a deck
        Deck deck = new Deck();
        //while there are still cards not dealt, deal them
        while (!deck.isEmpty()) {
            hands.get(1).put(deck.deal());
            hands.get(0).put(deck.deal());
        }
    }

    //play the game
    static void playWar(ArrayList<Queue<Card>> hands) {
        //the number of rounds
        long rounds = 0;
        String winner = "";
        //loop until either hand becomes empty, if one does, the other is the winner, if both does at same time, no one wins
        while (true) {
            if (hands.get(0).size() <= 0 && hands.get(1).size() <= 0) {
                winner = "No One";
                break;
            }
            if (hands.get(0).size() <= 0) {
                winner = "Player B";
                break;
            }
            else if (hands.get(1).size() <= 0) {
                winner = "Player A";
                break;
            }
            //when not empty, inc the round counter, print out the round counter. play one round
            else {
                rounds++;
                System.out.println(String.format("Round %s !.", rounds));
                playRound(hands);
            }
        }
        //print out the rounds played and the winner at the end of the game
        System.out.println(String.format("Played %d rounds.", rounds));
        System.out.println(String.format("%s is the winner!", winner));
    }

    //function to play a round
    static void playRound(ArrayList<Queue<Card>> hands) {
        //create 2 variables for the 2 cards that are placed into the field
        Card aCard = hands.get(0).remove();
        Card bCard = hands.get(1).remove();
        System.out.println("-------------------------");
        System.out.println(String.format("%s compares to %s", aCard.toString(), bCard.toString()));
        int compVal = aCard.compareTo(bCard);
        //compare the cards, if aCard is bigger, A WINS, if bCard is bigger B wins. when they win the cards go into the bottom of their hand
        if (compVal > 0) {
//            AWINS
            hands.get(0).put(bCard);
            hands.get(0).put(aCard);
        } else if (compVal < 0) {
//            BWINS
            hands.get(1).put(aCard);
            hands.get(1).put(bCard);
        } else {
//            TIE
            //create 2 stacks of cards for the 2 players, each put down their first tied card into it
            Stack<Card> aBoardCards = new Stack<>();
            Stack<Card> bBoardCards = new Stack<>();
            aBoardCards.put(aCard);
            bBoardCards.put(bCard);
            //call war
            war(hands, aBoardCards, bBoardCards);
        }
        System.out.println(String.format("Player A has %d, Player B has %d", hands.get(0).size(), hands.get(1).size()));
        System.out.println("-------------------------\n");
    }

    //function for war
    static void war(ArrayList<Queue<Card>> hands, Stack<Card> aBoardCards, Stack<Card> bBoardCards) {
        System.out.println("War!");
        //if either player has no cards left, this returns without doing anything, the game will end at the hand size checking before the start of the next round
        if ((hands.get(0).size() < 0) || (hands.get(1).size() < 0)) {
            return;
        }
        //At this point, there is at least one card in both hands. Game continues even if the player does not have 4 cards to put down for war, so it adds cards onto their respective board stacks if one exist on their hand
        for (int ecx=0; ecx < 4; ecx++) {
            if (hands.get(0).size() > 0)
                aBoardCards.put(hands.get(0).remove());
            if (hands.get(1).size() > 0)
                bBoardCards.put(hands.get(1).remove());
        }
        //compare the first cards of both boards stacks, this is the last put on card
        int compVal = aBoardCards.peek().compareTo(bBoardCards.peek());
        System.out.println(String.format("%s compares to %s", aBoardCards.peek().toString(), bBoardCards.peek().toString()));
        //if A wins, a get the cards, if B wins, B get the cards, if no one wins, then recursively call war
        if (compVal > 0) {
            claimCards(hands.get(0), aBoardCards, bBoardCards);
        } else if (compVal < 0) {
            claimCards(hands.get(1), aBoardCards, bBoardCards);
        } else {
            war(hands, aBoardCards, bBoardCards);
        }
    }

    //helper function to claim the cards won in war
    static void claimCards(Queue<Card> hand, Stack<Card>... boadCardss) {
        for (Stack<Card> boadCards : boadCardss) {
            for (Card c : boadCards) {
                hand.put(c);
            }
        }
    }
}